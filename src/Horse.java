public class Horse extends Animal{
    public Horse(String name) {
        super(name);
        species = "horse";
        numLegs = 4;

    }
}