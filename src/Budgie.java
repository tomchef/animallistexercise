public class Budgie extends Animal{
    public Budgie(String name) {
        super(name);
        species = "bird";
        numLegs = 2;

    }
}