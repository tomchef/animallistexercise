import java.util.ArrayList;

public class AnimalListExercise {

    ArrayList<Animal> beasts = new ArrayList<>();

    public static void main(String[] args) {
        AnimalListExercise ale = new AnimalListExercise();
        ale.populateBeastList();
        ale.getList();
    }

    private void getList() {
        for (Animal i : beasts) {
            System.out.println(i.describe());
        }
    }

    private void populateBeastList() {
        Snake jafar = new Snake("Jafar");
        Horse rosinante = new Horse("Rosinante");
        Budgie urbino = new Budgie("Urbino");
        beasts.add(jafar);
        beasts.add(rosinante);
        beasts.add(urbino);
    }
}
