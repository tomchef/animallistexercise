import java.util.ArrayList;

public class Animal {



    protected String name;
    protected String species;
    protected int numLegs;

    public Animal(String name, String species, int numLegs)
    {
        this.name = name;
        this.species = species;
        this.numLegs = numLegs;
    }

    public Animal(String name) {
        this.name = name;
    }


   public String describe() {
        String myStr = ("I'm " + name + ", a " + species + " and I have " + numLegs + " legs" + '\n');
       return myStr.replaceAll("0", "no");
   }


}
